import 'dart:async';
import 'package:adminapp/InvoiceWeb.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'Database/Helper.dart';
import 'Invoice.dart';
class Invoices extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Invoices();
  }
}
class _Invoices extends State<Invoices> {
  Helper h=new Helper();
  String url2= "file:///android_asset/flutter_assets/assets/html/ChartPage.html";
  bool add_btn=true;
  String url="";
  void Bindurl() async  {
    var session = await h.Getsession();
    url=h.hostsv+"MobileLayout/Invoices?view=sale";
    setState((){
    });
  }


  @override
  void initState(){
    Bindurl();
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    Completer<WebViewController> _controller = Completer<WebViewController>();

    JavascriptChannel _receivingdata(BuildContext context){
      return JavascriptChannel(
          name:"enableAdd_btn",
          onMessageReceived: (JavascriptMessage message){
            setState(() {
              add_btn=true;
            });
          }
      );
    }
     return
      Scaffold(
        body:(url=="")?null:new WebView(
          initialUrl:url,
          javascriptMode: JavascriptMode.unrestricted,
          javascriptChannels: <JavascriptChannel>[
            _receivingdata(context),
          ].toSet(),
          onWebViewCreated: (WebViewController webViewController) {
             _controller.complete(webViewController);

          },
        ),
          floatingActionButton:
    new Row(
      children: <Widget>[
        new Padding(
          padding: new EdgeInsets.symmetric(
            horizontal: 15.0,
          ),
        ),

        Flexible(fit: FlexFit.tight, child: SizedBox()),
        (add_btn)?
        new FloatingActionButton(
            elevation: 0.0,
            heroTag: "btn2",
            child: new Icon(Icons.add),
            backgroundColor: new Color(0xFFE57373),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>new InvoiceWeb()),
              );
            }
        ):Text(""),
      ],
    )

      );

  }
}