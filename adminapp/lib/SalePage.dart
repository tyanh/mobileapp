import 'dart:convert';

import 'package:adminapp/ApiApp.dart';
import 'package:adminapp/Controls/ItemBill.dart';
import 'package:adminapp/Controls/ItemSearchInvoice.dart';
import 'package:adminapp/Controls/SumSearch.dart';
import 'package:adminapp/Controls/buttonCtr.dart';
import 'package:adminapp/Controls/loaderCtr.dart';
import 'package:adminapp/Invoice.dart';
import 'package:flutter/material.dart';

import 'Controls/ListTileSale.dart';
class SalePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SalePage();
  }

}
class _SalePage extends State<SalePage> {
  bool load=false;
  bool search_=false;
  ApiApp api=new ApiApp();
  List<ItemSearchInvoice> l=new List<ItemSearchInvoice>();
 void getDataList() async{
    var a=await api.getDatalist("billsales", "0", "50", "*");
    var j=jsonDecode(a);
    int order=1;
    for(var i in j)
      {
        ItemSearchInvoice b=new ItemSearchInvoice();
        b.myctr.order=order;
        b.myctr.phone="09087667765";
        b.myctr.NumInvoice="1212121";
        b.myctr.name="Nuyen Van A";
        b.myctr.summoney=30000000;
        b.myctr.money=10000000;
        b.myctr.member="Customer";
        b.myctr.qlt=10;
        l.add(b);
        order++;
       }
    setState(() {
      load=true;
    });
  }
  @override
  void initState()  {
    super.initState();
    getDataList();
  }
  Widget CtrSearch(){
   return Container(
       child:
       Padding(
         padding: const EdgeInsets.all(8.0),
         child: Column(
           children: <Widget>[
             Padding(
               padding: const EdgeInsets.all(8.0),
               child: TextField(
                 keyboardType: TextInputType.number,
                 decoration: InputDecoration(
                   labelText: 'Số hóa đơn',
                 ),
               ),
             ),
             Padding(
               padding: const EdgeInsets.all(8.0),
               child: TextField(
                 keyboardType: TextInputType.number,
                 decoration: InputDecoration(
                   labelText: 'Số điện thoại',
                 ),
               ),
             ),
             Padding(
               padding: const EdgeInsets.all(8.0),
               child: TextField(
                 keyboardType: TextInputType.number,
                 decoration: InputDecoration(
                   labelText: 'Họ tên',
                 ),
               ),
             ),
             Padding(
               padding: const EdgeInsets.all(8.0),
               child: Container(
                 width: double.infinity,
                 color: Colors.blue,
                 child:
                 MaterialButton(
                   height: 30.0,
                   child: new Text('search',
                       style:
                       new TextStyle(fontSize: 16.0, color: Colors.white)),
                 ),
               ),
             )

           ],
         ),
       )


   );
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Scaffold(
      body: load?Container(
          child:!search_?Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                Container(
                  margin: const EdgeInsets.fromLTRB(5,0,5,5.0),
                  padding: const EdgeInsets.all(3.0),
                  decoration: BoxDecoration(

                      border: Border.all(color: Colors.blueAccent)
                  ),
                  child: new SumSearch(),
                )

              ),
              Expanded(
                child: ListView(
                  children: l,
                ),
              )
            ],
          ):CtrSearch(),
        )
        :Center(
          child: new loaderCtr(),
        ),
        floatingActionButton:!search_?new Row(
          children: <Widget>[
            new Padding(
              padding: new EdgeInsets.symmetric(
                horizontal: 15.0,
              ),
            ),
            new FloatingActionButton(
              heroTag: "btn1",
              onPressed:() {
                setState(() {
                  search_=true;
                });
              },
              child: new Icon(Icons.search),
            ),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            new FloatingActionButton(
                elevation: 0.0,
                child: new Icon(Icons.add),
                backgroundColor: new Color(0xFFE57373),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>new Invoice()),
                  );
                }
            )
          ],
        ):null,

    );


  }

}