import 'package:adminapp/Chat.dart';
import 'package:adminapp/Home.dart';
import 'package:adminapp/Items.dart';
import 'package:adminapp/ListChat.dart';
import 'package:adminapp/socketIoClient.dart';
import 'package:adminapp/webview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'ApiApp.dart';
import 'Controls/DonutPieChart.dart';
import 'SimpleBarChart.dart';
import 'Profile.dart';
import 'SalePage.dart';
import 'Todolist.dart';
import 'package:badges/badges.dart';
class Indext extends StatefulWidget {

  Indext();
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Indext();
  }
}
class _Indext extends State<Indext> {
  //socketIoClient sk=new socketIoClient();
  String menu="";
  String img ="";
  int _selectedIndex =0;
  bool isprofile=false;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  @override
  Widget build(BuildContext context) {
    Color colorIcon = Colors.black;
    double sizeicon=30.0;
    var api=new ApiApp();
     var a=DrawerHeader(
      child: Image.network(api.imgLink+"/logo.png",width: 25),
      decoration: BoxDecoration(
        color: Colors.blue,
      ),
    );

    List<BottomNavigationBarItem> btmenu = new List<BottomNavigationBarItem>();
    btmenu.add(BottomNavigationBarItem(
      icon: Icon(Icons.home),
      title: Text('Home'),
    ),
    );

    btmenu.add(BottomNavigationBarItem(

      icon: Badge(
        badgeContent: Text("1"),
        showBadge:false,
        child: Icon(Icons.chat),
      ),
      title: Text('Sale'),

    ),
    );

    String title="Monaco";
    List<Widget> lmenus=new List<Widget>();
    lmenus.add(a);

    List<Widget> _widgetOptions = <Widget>[
      webview("http://192.168.1.113/html/ChartPage.html"),
      Home(),

    ];
    return MaterialApp(
        home:Scaffold(
          appBar: AppBar(title: Text(title),
            actions: <Widget>[

            ],
          ),
          body:Center(
            child: _widgetOptions.elementAt(_selectedIndex),
          ),
          drawer: Drawer(
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the drawer if there isn't enough vertical
            // space to fit everything.
            child: ListView(
              // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: lmenus
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _selectedIndex ,
            items:
            btmenu
            ,
            selectedItemColor: Colors.blue,
            onTap: _onItemTapped,
          ),

        )
    );

  }

}