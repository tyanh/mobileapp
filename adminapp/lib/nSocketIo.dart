import 'dart:convert';
import 'dart:io';

import 'package:adminapp/ApiApp.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
class nSocketIo {
  String urlsk = "http://192.168.137.1:3000";
  SocketIOManager manager = SocketIOManager();
  SocketIO socket;
  // Dart client
  nSocketIo(){
    _onInitSocketIO();
  }

Future _onInitSocketIO() async {
    socket = await manager.createSocketIO(urlsk, "/", query: "newconnect",socketStatusCallback: _onSocketStatus);
    socket.init();
    socket.connect();

  }

  Future sendmessess(String channel,String content) async {
    SharedPreferences user_ = await SharedPreferences.getInstance();
    Map o=new Map();
    o["username"]=user_.getString('user_name');
    o["content"]=content;
    await socket.sendMessage(channel,o.toString(),_onReceiveChatEvent);
  }

  void _onSocketStatus(dynamic data) async {
    // If socket connects successfully
    if (data == "connect") {
      // Send message to server on the 'chat' event.
      SharedPreferences user_ = await SharedPreferences.getInstance();
      Map o=new Map();
      o["username"]=await user_.getString('user_name');
      o["groupchat"]="messages";
      socket.sendMessage('joinGroup', o.toString());
      // Subscribe to the 'chat' event.
      socket.subscribe("messages", _onReceiveChatEvent);
    }
  }

  void _onReceiveChatEvent(dynamic data) {
    print("_onReceiveChatEvent");
    //print(data);
  }
}
