import 'dart:convert';
import 'package:flutter/material.dart';

import 'ApiApp.dart';
import 'Database/SqlData.dart';
import 'PageHome.dart';

class Login extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginPage();
  }
}
class LoginPage extends State<Login>{

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  AssetImage img=AssetImage('assets/imgs/logo.png');
  var email = TextEditingController();
  FocusNode emailFocusNode;
  bool btn_login=true;
  var password = TextEditingController();
  Widget btn_;
  String txtlogin = "";
  @override
  // TODO: implement widget
  Widget build(BuildContext context){

    if(btn_login){
      btn_=new MaterialButton(
        minWidth: double.infinity,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          LoginApp();
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      );
    }
    else
      {
        btn_=CircularProgressIndicator(
          strokeWidth: 5.0,

        );
      }
    return new MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
           children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new GestureDetector(
                onTap:() {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                  child: Image(image: img)
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                focusNode: emailFocusNode,
                controller: email,
                obscureText: false,
                decoration: InputDecoration(
                    focusColor: Colors.red,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Email",
                    border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
              ),
            ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: password,
            obscureText: true,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                hintText: "Password",
                border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
          ),
        ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child:new Text(
            txtlogin
        ),
      ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 5.0,
            borderRadius: BorderRadius.circular(30.0),
            color: Color(0xff01A0C7),
            child:btn_
          ),
        )
          ],

       ),
     ),

    );
  }
  void LoginApp() async{

    String email_=email.text;
    String pass_=password.text;
    setState(()=>txtlogin="");
    if(email_==""){
      //FocusScope.of(context).requestFocus(emailFocusNode);
      setState(()=>txtlogin="Email?");
      return;
    }
    if(pass_==""){
      //FocusScope.of(context).requestFocus(emailFocusNode);
      setState(()=>txtlogin="pass_?");
      return;
    }
    setState(() {
      btn_login = false;
    });

    var api=new ApiApp();
      var lg=await api.getDataLogin(email_,pass_);

    if(lg!="-1" && lg!="-2")
    {
      var rs=json.decode(lg) as List;
      Map note=new Map();
      note["user"]=email_;
      note["pass"]=pass_;
      note["id"]=1;
      note["iddata"]=rs[0]["id"];
      var db=await SqlData();
      await db.updateNote(note);
      runApp(MaterialApp(
            home: PageHome(json.encode(rs[1]),rs[0]["img"]),
      ));
      return;
    }
    else
      {
        if(lg=="-2")
          lg="Kiểm tra lại kết nối mạng";
        setState(() {
          txtlogin=lg;
          btn_login = true;
        });
      }
  }

}
