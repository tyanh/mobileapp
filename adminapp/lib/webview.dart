import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'Database/Helper.dart';
import 'Invoice.dart';
class webview extends StatelessWidget {
  Helper h=new Helper();
  String url= "";
  webview(this.url);
  JavascriptChannel _receivingdata(BuildContext context){
    return JavascriptChannel(
      name:"rcvData",
      onMessageReceived: (JavascriptMessage message){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>new Invoice()),
        );
      }
    );
  }
  @override
  Widget build(BuildContext context) {
    print(h.hostsv+ url);
    Completer<WebViewController> _controller = Completer<WebViewController>();
    return
      Scaffold(
          body:new WebView(
            initialUrl:h.hostsv+ url,
            javascriptMode: JavascriptMode.unrestricted,
            javascriptChannels: <JavascriptChannel>[
              _receivingdata(context),
            ].toSet(),
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
          )

      );

  }
}
