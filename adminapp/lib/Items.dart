import 'dart:async';
import 'dart:convert';

import 'package:adminapp/ApiApp.dart';
import 'package:adminapp/ScanCode.dart';
import 'package:flutter/material.dart';
class Items extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Items();
  }
}

class _Items extends State<Items> {
  ApiApp api=new ApiApp();
  bool search=false;
  StreamController<String> _events=new StreamController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
      Scaffold(
        body:search?
        Center(
        child:CircularProgressIndicator(
          strokeWidth: 5.0,

        )):ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.map),
              title: Text('Map'),
            ),
            ListTile(
              leading: Icon(Icons.photo_album),
              title: Text('Album'),
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text('Phone'),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            var scan=ScanCode();
            String code=await scan.scanBarcodeNormal();

            var data=await api.getItemScan(code, "products");
            Map<String, dynamic> datas = jsonDecode(data);
            setState(() {
              search=true;
                if(datas.length>0)
                  search=false;
            });
            // Add your onPressed code here!
          },
          child: Icon(Icons.camera),
          backgroundColor: Colors.green,
        ),
      );
  }

}