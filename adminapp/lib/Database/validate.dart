import 'dart:convert';
import 'dart:io';

import 'package:adminapp/Controls/TextInput.dart';
import 'package:adminapp/Controls/imageUpload.dart';
import 'package:flutter/material.dart';

class validate{
  String CheckData(List<Map> maps,List<Widget> Wgets){

    for(var t=0;t<maps.length;t++){
      String valid=maps[t]["valid"].toString();
      if(Wgets[t] is TextInput){
        TextInput txtCtr=Wgets[t] as TextInput;
        maps[t]["data"]=txtCtr.control.text;
      }

      if(valid=="true")
       {
         String type=maps[t]["type"].toString();
         String title=maps[t]["note"].toString();

         if(type=="img")
         {
           imageUpload txtCtr=Wgets[t] as imageUpload;
           if(txtCtr.myCtr.getFile()==null)
             return t.toString();

         }
         if(type=="text")
         {
           TextInput txtCtr=Wgets[t] as TextInput;
           if(txtCtr.control.text=="")
             return t.toString();


         }
         if(type=="password")
         {
           TextInput txtpass=Wgets[t] as TextInput;
           if(txtpass.control.text=="")
             return t.toString();

         }
         if(type=="passwordagain")
         {
           TextInput txtpass=Wgets[t-1] as TextInput;
           TextInput txtpass2=Wgets[t] as TextInput;
           if(txtpass.control.text!=txtpass2.control.text)
             return t.toString();

         }
         if(type=="email")
         {
           TextInput txtCtr=Wgets[t] as TextInput;
           if(!isEmail(txtCtr.control.text))
             return t.toString();

         }
       }
    }

    return json.encode(maps);
  }
  bool isEmail(String email){
    bool emailValid = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
    return emailValid;
  }
}