import 'dart:async';
import 'dart:io' as io;
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:fluttertoast/fluttertoast.dart';
class SqlData{
  static final SqlData _instance = new SqlData.internal();

  factory SqlData() => _instance;

  final String tableNote = 'loginTb';
  final String columnId = 'id';
  final String columnTitle = 'user';
  final String columnDescription = 'pass';

  static Database _db;

  SqlData.internal();

  Future<Database> get db async {
     if (_db != null) {
      return _db;
    }

    _db = await initDb();

    return _db;
  }

  initDb() async {
//io.Directory dcmdirectory=await getApplicationDocumentsDirectory();
    String databasesPath = await getDatabasesPath();
    //String databasesPath ="/data/data/com.minhnhat.adminapp/databases/";
    String path = join(databasesPath, 'data_app.db');
    //await deleteDatabase(path); // just for testing

      var db = await openDatabase(path, version: 1, onCreate: _onCreate);
      return db;
  }

  void _onCreate(Database db, int newVersion) async {
    await db.execute(
        'CREATE TABLE $tableNote($columnId INTEGER PRIMARY KEY AUTOINCREMENT, $columnTitle TEXT, $columnDescription TEXT,iddata INTEGER)');
  }

  Future<int> saveNote(Map note) async {
    var dbClient = await db;
   // var result = await dbClient.insert(tableNote, note);
    print(note);
   var result = await dbClient.rawInsert(
        'INSERT INTO $tableNote ($columnTitle, $columnDescription) VALUES (\'${note["user"]}\', \'${note["pass"]}\')');
    return result;
  }

  Future<List> getAllNotes() async {
    var dbClient = await db;
    var result = await dbClient.query(tableNote, columns: [columnId, columnTitle, columnDescription]);
//    var result = await dbClient.rawQuery('SELECT * FROM $tableNote');

    return result.toList();
  }

  Future<int> getCount() async {
    var dbClient = await db;
    return Sqflite.firstIntValue(await dbClient.rawQuery('SELECT COUNT(*) FROM $tableNote'));
  }

  Future<Map> getNote(int id) async {
     var dbClient = await db;
    ////List<Map> result = await dbClient.query(tableNote,
        //columns: [columnId, columnTitle, columnDescription],
        //where: '$columnId = ?',
        //whereArgs: [id]);
    var result = await dbClient.rawQuery('SELECT * FROM $tableNote WHERE $columnId = $id');

    if (result.length > 0) {
      return result[0];
    }

    return null;
  }

  Future<int> deleteNote(int id) async {
    var dbClient = await db;
    return await dbClient.delete(tableNote, where: '$columnId = ?', whereArgs: [id]);
//    return await dbClient.rawDelete('DELETE FROM $tableNote WHERE $columnId = $id');
  }

  Future<int> updateNote(Map note) async {
    var dbClient = await db;
  //  return await dbClient.update(tableNote, note, where: "$columnId = ?", whereArgs: [note["id"]]);
    return await dbClient.rawUpdate(
        'UPDATE $tableNote SET $columnTitle = \'${note["user"]}\', $columnDescription = \'${note["pass"]}\', iddata = \'${note["iddata"]}\' WHERE $columnId = ${note["id"]}');
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
  void alert(String txt){
    Fluttertoast.showToast(
        msg:txt,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 5,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }
}