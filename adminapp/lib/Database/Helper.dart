import 'package:shared_preferences/shared_preferences.dart';

class Helper{
  final String hostsv5="http://192.168.100.5/";
  String hostsv="http://192.168.137.1/";
  String convertNumberToString(String nb){
    String o="";
    int m=0;
    int l=nb.length-1;
    for(var i=l;i>=0;i--){
      o=o+nb[m];
      if(i%3==0 && i>0)
        {
          o=o+",";
        }
      m++;
    }

    return o.toString();
  }
  String convertStringToNumber(String nb){
     return nb.replaceAll(",", "");
  }
  int convertStringToInt(String nb){
    try {
      return int.parse(nb.replaceAll(",", ""));
    }
    catch(ex){
      return 0;
    }
  }
 Future<String> Getsession() async {
    var session = await SharedPreferences.getInstance();
     return await session.getString('session').toString();

  }
}