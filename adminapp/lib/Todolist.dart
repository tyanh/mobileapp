import 'dart:convert';

import 'package:adminapp/ApiApp.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
class Todolist{
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  Todolist(){
    var initializationSettingsAndroid =
    new AndroidInitializationSettings('logo');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }
  var api=new ApiApp();
  void GetList() async{
    var l=await api.getTodolist();
    if(l=="0")
      return;
    var todo=json.decode(l) as List;
    int bg=10;
    for(var i in todo){
     await awaitPush(i["name_"].toString(), i["contend"].toString(), bg);
     bg=bg+5;
    }

  }
  Future onSelectNotification(String payload) async {

  }
  Future awaitPush(String title,String body,int time) async{

    var scheduledNotificationDateTime =
    new DateTime.now().add(new Duration(seconds: time));
    print(scheduledNotificationDateTime);
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        playSound: false, importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics =
    new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.schedule(
        0,
        title,
        body,
        scheduledNotificationDateTime,
        platformChannelSpecifics);

  }
}