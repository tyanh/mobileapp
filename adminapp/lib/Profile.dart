import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:adminapp/ApiApp.dart';
import 'package:adminapp/Database/validate.dart';
import 'package:adminapp/Login.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Controls/TextInput.dart';
import 'Controls/buttonCtr.dart';
import 'Controls/imageUpload.dart';
import 'Database/SqlData.dart';
class Profile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoadProfile();
  }
}
class LoadProfile extends State<Profile> {
  var api=new ApiApp();
  var valid=new validate();
  List<Map> dataPush=new List<Map>();
  bool finish=false;
  String note_="";
  List<Widget> Lctrs=new List<Widget>();
  List<Widget> LctrsTxt=new List<Widget>();
  List<TextEditingController> txtcontroler=new List<TextEditingController>();
  String p="";
  var file=new imageUpload();
  void getProfile() async{
    p=await api.getProfileLogin();

    var Lctr= json.decode(p) as List;

    List<Widget> list = Lctr.map((value) {
      Map o=new Map();
      o["valid"]=value["valid"].toString();
      o["column"]=value["column"].toString();
      o["title"]=value["title"].toString();
       o["type"]=value["type"].toString();
      o["note"]=value["note"].toString();
      o["data"]="";
      dataPush.add(o);
      if(value["type"].toString()=="img") {
        if(value["data"].toString()!="") {
            file.myCtr.link=api.imgRoot+value["data"].toString();
        }
         return file;
      }
      else
        {
          bool obscureText=false;
          if(value["type"].toString()=="password" || value["type"].toString()=="passwordagain"){
            obscureText=true;
          }
          String data=value["data"].toString();
          TextEditingController txtCtr=new TextEditingController();
          txtCtr.text=data;
          txtcontroler.add(txtCtr);
          var ctr=TextInput(value["title"], txtCtr, obscureText);
          LctrsTxt.add(ctr);
          return ctr;
        }
    }).toList();
    Lctrs.addAll(list);

    Widget btnlogout=new GestureDetector(
      onTap: () async {
        Map note=new Map();
        note["user"]="";
        note["pass"]="";
        note["id"]=1;
        var db=await SqlData();
        await db.updateNote(note);
        runApp(Login());
      },
      child: new buttonCtr("Logout"),
    );

   setState(() {
      finish=true;
    });
  }
  void Logout() async{
    Map note=new Map();
    note["user"]="";
    note["pass"]="";
    note["id"]=1;
    await api.getDataLogout();
    var db=await SqlData();
    await db.updateNote(note);
    runApp(Login());
  }
  @override
  void initState() {
    super.initState();
    getProfile();
  }
bool load=false;
  @override
  Widget build(BuildContext context) {
    Widget btn_=new GestureDetector(
      onTap: () async {
        File _file=file.myCtr.getFile();
        var checkdata=valid.CheckData(dataPush, Lctrs);
        try{
          setState(() {
            note_ =dataPush[int.parse(checkdata)]["note"];
          });
        }
        catch(ex){
          setState(() {
            note_="";
           load = true;
          });

          api.UploadProfile(_file,checkdata);
          Logout();
        }
       },
      child: new buttonCtr("Save"),

    );

    // TODO: implement build
   return Scaffold(

     appBar: AppBar(
       title: Text("Profile"),
       actions: <Widget>[
         IconButton(
           icon: Icon(Icons.arrow_right ),
           onPressed: () async {
             Logout();
           },
         ),
       ],
     ),
      body:finish?Scaffold(
        body:SingleChildScrollView(
          child:Column(
            children: <Widget>[
              file,
              new Column(
                children:
                  LctrsTxt,

              ),
              Text(note_),
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0,0,0,8.0),
                child: !load?btn_:CircularProgressIndicator(
                  strokeWidth: 5.0,

                ),
              ),
            ],
          ) ,
        )
      ):
      Center(

           child: CircularProgressIndicator(
             strokeWidth: 5.0,

           ),

     ),

    );
  }
}
