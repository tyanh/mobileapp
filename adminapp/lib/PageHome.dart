import 'package:adminapp/Chat.dart';
import 'package:adminapp/Home.dart';
import 'package:adminapp/Invoice.dart';
import 'package:adminapp/Invoices.dart';
import 'package:adminapp/Items.dart';
import 'package:adminapp/ListChat.dart';
import 'package:adminapp/socketIoClient.dart';
import 'package:adminapp/viewcommon.dart';
import 'package:adminapp/webview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'ApiApp.dart';
import 'Controls/DonutPieChart.dart';
import 'SimpleBarChart.dart';
import 'Profile.dart';
import 'SalePage.dart';
import 'Todolist.dart';
import 'package:badges/badges.dart';
class PageHome extends StatefulWidget {
  final appTitle = 'Drawer Demo';
  String menu ="";
  String img ="";
  PageHome(this.menu,this.img);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoadContend(menu,img);
  }
}
class LoadContend extends State<PageHome> {
  //socketIoClient sk=new socketIoClient();
  String menu="";
  String img ="";
  LoadContend(this.menu,this.img);
  int _selectedIndex =0;
  bool isprofile=false;
   void _onItemTapped(int index) {
     setState(() {
      _selectedIndex = index;
    });
  }
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  @override
  Widget build(BuildContext context) {
    Color colorIcon = Colors.black;
    double sizeicon=30.0;
    var api=new ApiApp();
    var menus=json.decode(menu) as List;
    var a=DrawerHeader(
      child: Image.network(api.imgLink+"/logo.png",width: 25),
      decoration: BoxDecoration(
        color: Colors.blue,
      ),
    );
    List<ListTile> list = menus.map((value) {
      return ListTile(
        leading:value["img"]!=""?Image.network(api.imgLink+value["img"].toString()+".png",width: 25):Text(""),
        title: Text(capitalize(value["name"].toString())),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>new viewcommon("MobileLayout/ViewProduct?user=")),
          );

        },
      );
    }).toList();
    List<BottomNavigationBarItem> btmenu = new List<BottomNavigationBarItem>();
    btmenu.add(BottomNavigationBarItem(
      icon: Icon(Icons.home),
      title: Text('Home'),
    ),
    );
    menus.map((value) {
      if(value["btnmobile"].toString()=="checked") {
        btmenu.add(BottomNavigationBarItem(
          icon: Image.network(api.imgLink+value["img"].toString()+".png",width: 25),
          title: Text(capitalize(value["name"].toString())),
          //activeIcon: Image.network(api.imgLink+value["img"].toString()+".png",width: 25,color:_selectedIndex==1?Colors.blue : null),
          activeIcon: Image.network(api.imgLink+value["img"].toString()+".png",width: 25,color:Colors.blue),
        ),
        );
      }
    }).toList();
    btmenu.add(BottomNavigationBarItem(

      icon: Badge(
        badgeContent: Text("1"),
        showBadge:false,
        child: Icon(Icons.chat),
      ),
      title: Text('Sale'),

    ),
    );
    btmenu.add(BottomNavigationBarItem(
      icon: Icon(Icons.assessment),
      title: Text('Scan'),

    ),
    );
    String title="Monaco";
    List<Widget> lmenus=new List<Widget>();
    lmenus.add(a);
    lmenus.addAll(list);
    String avt=api.imgRoot+img;
    List<Widget> _widgetOptions = <Widget>[
      Home(),
      Invoices(),
      Items(),

    ];
    return MaterialApp(
        home:Scaffold(
          appBar: AppBar(title: Text(title),
          actions: <Widget>[
            IconButton(
                 icon: CircleAvatar(
                   radius: 15.0,
                   backgroundImage:NetworkImage(avt),

                 ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>new Profile()),
                );
                setState(() {
                  isprofile = true;
                });
              },
            ),
          ],
          ),
          body:Center(
            child: _widgetOptions.elementAt(_selectedIndex),
          ),
          drawer: Drawer(
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the drawer if there isn't enough vertical
            // space to fit everything.
            child: ListView(
              // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: lmenus
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _selectedIndex ,
            items:
            btmenu
            ,
            selectedItemColor: Colors.blue,
            onTap: _onItemTapped,
          ),

        )
    );

  }

}