import 'dart:convert';

import 'package:adminapp/ApiApp.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import 'ChatBloc.dart';
import 'Controls/Userchat.dart';
import 'ListChatBloc.dart';
class socketIoClient {
  IO.Socket socket;
  ChatBloc Chats = ChatBloc();
  ListChatBloc Group = ListChatBloc();
  ApiApp api=new ApiApp();
  socketIoClient() {
    socket = IO.io('http://192.168.137.1:3000', <String, dynamic>{
      'transports': ['websocket'],
    });
    socket.on('connect', (_) async {
      SharedPreferences user_ = await SharedPreferences.getInstance();
      SharedPreferences idprofile = await SharedPreferences.getInstance();
      Map o=new Map();
      o["username"]=await user_.getString('user_name');
      o["iduser"]=await idprofile.getInt('idprofile').toString();
      o["groupchat"]="admin";
      socket.emit('joinGroup',jsonEncode(o));
    });
    socket.on('thread', (data) => {
      bindcontentChat(data)
    });
    socket.on('newconnect', (data) => {
      bindnewConnect(data,true)
     });
    socket.on('outconnect', (data) => {
      logout(data)
    });
    socket.on('disconnect', (_) => print('disconnect'));
    //socket.on('fromServer', (_) => print(_));
  }
  bindcontentChat(String data){
    logout("0");
    //Chats.addFriendNote(data);
  }
  logout(String index){
    print("111");
    print( Group.notes[0]);
    Group.notes[0].myuChat.updateItem(false,"11","11");
    return;
    print("out:"+index);

      for(var i=0;i<Group.notes.length;i++){
      var o=Group.notes[i];
      if(o.myuChat.idclient.toString()==index){
        print("aaa:"+i.toString());
        Group.update(i,false,"","");
        return;
      }
    }
  }
  bindnewConnect(String data,bool st) async{
    var j=jsonDecode(data);
    String idpr=await api.GetIdLogin();
    if(idpr==j["iduser"].toString()) {
         return;
    }
    for(var i=0;i<Group.notes.length;i++){
      var o=Group.notes[i];
      if(o.myuChat.iduser.toString()==j["iduser"].toString()){
        Group.update(i,true,j["idclient"].toString(),j["ipclient"].toString());
        return;
      }
    }
    Group.addNote(j["name"].toString(),j["idclient"].toString(),j["ipclient"].toString(),"0",j["iduser"].toString());
  }
  sendmess(String channel,String content) async{
    print(content);
    if(content=="")
      return;

    Map o=new Map();
    SharedPreferences user_ = await SharedPreferences.getInstance();
    o["username"]=user_.getString('user_name');
    o["content"]=content;
    socket.emit(channel,jsonEncode(o));
    Chats.addMeNote(content);
  }
}