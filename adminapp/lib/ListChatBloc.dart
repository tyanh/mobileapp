import 'dart:async';
import 'dart:convert';
import 'ApiApp.dart';
import 'Controls/Userchat.dart';
class ListChatBloc {
  final notes =List<Userchat>();
  final _controller = StreamController<List<Userchat>>.broadcast();
  get controllerOut => _controller.stream.asBroadcastStream();
  get controllerIn => _controller.sink;
  ApiApp api=new ApiApp();
  update(int index,bool st,String idclient,String ipclient)
  {
    print("inex:"+index.toString());
    print("inex2:"+notes.length.toString());
    notes[index].myuChat.updateItem(st, idclient, ipclient);
    //notes[index].myuChat.online=st;
  }
  addNote(String txt,String idclient,String ipclient,String status,String iduser) {
    var c=new Userchat();
    c.myuChat.name=txt;
    c.myuChat.idclient=idclient;
    c.myuChat.ipclient=ipclient;
    c.myuChat.iduser=iduser;
    if(status=="0")
      c.myuChat.online=true;
    else
      c.myuChat.online=false;
    notes.add(c);
    controllerIn.add(notes);
  }
  initdata()  {
    var c =  new Userchat();
    notes.add(c);
    controllerIn.add(notes);

  }
  void dispose() {
    print("dispose");
    _controller.close();
  }
}
