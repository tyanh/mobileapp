enum State {
  login, count, list
}

abstract class UiState {
  State state;

  UiState(this.state);

  @override
  String toString() => "State: $state";
}

