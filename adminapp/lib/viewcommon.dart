import 'dart:async';
import 'package:adminapp/Controls/loaderCtr.dart';
import 'package:adminapp/InvoiceWeb.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'Database/Helper.dart';

class viewcommon extends StatefulWidget {
  String urls="";
  viewcommon(this.urls);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _viewcommon(urls);
  }
}
class _viewcommon extends State<viewcommon> {
  Helper h=new Helper();
  bool add_btn=false;
  String url="";
  bool load=false;
  _viewcommon(this.url);
  void Bindurl() async  {
    var session = await h.Getsession();
    setState((){
      url=h.hostsv+url+session;
      load=true;
    });
  }
  @override
  void initState(){
    Bindurl();
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    Completer<WebViewController> _controller = Completer<WebViewController>();
    JavascriptChannel _receivingdata(BuildContext context){
      return JavascriptChannel(
          name:"enableAdd_btn",
          onMessageReceived: (JavascriptMessage message){
            setState(() {
              add_btn=true;
            });
          }
      );
    }

    return
      Scaffold(
        appBar: AppBar(
        title: Text("Sản phẩm"),
        ),
          body:(!load)?Center(child:loaderCtr(),):new WebView(
            initialUrl:url,
            javascriptMode: JavascriptMode.unrestricted,
            javascriptChannels: <JavascriptChannel>[
              _receivingdata(context),
            ].toSet(),
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);

            },
          ),
          floatingActionButton:
          new Row(
            children: <Widget>[
              new Padding(
                padding: new EdgeInsets.symmetric(
                  horizontal: 15.0,
                ),
              ),

              Flexible(fit: FlexFit.tight, child: SizedBox()),
              (add_btn)?
              new FloatingActionButton(
                  elevation: 0.0,
                  heroTag: "btn2",
                  child: new Icon(Icons.add),
                  backgroundColor: new Color(0xFFE57373),
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>new InvoiceWeb()),
                    );
                  }
              ):Text(""),
            ],
          )

      );

  }
}