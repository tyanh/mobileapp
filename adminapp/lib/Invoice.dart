import 'package:adminapp/ApiApp.dart';
import 'package:adminapp/Controls/FormPay.dart';
import 'package:adminapp/Controls/ItemBill.dart';
import 'package:adminapp/Controls/loaderCtr.dart';
import 'package:adminapp/Home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Controls/CircleImg.dart';
import 'Controls/HeaderInvoice.dart';
import 'Controls/ItemBillBloc.dart';
import 'Controls/CustomerInvoice.dart';
import 'Controls/LineGif.dart';
import 'Controls/TotalBill.dart';
import 'Controls/buttonCtr.dart';
import 'Database/Helper.dart';
import 'ScanCode.dart';

class Invoice extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Invoice();
  }
}


class _Invoice extends State<Invoice> {
  bool payment=false;
  var pay=new FormPay();
  var api=new ApiApp();
  ScrollController _controller;
  ItemBillBloc items = ItemBillBloc();
  Totalbill ttinvoice=new Totalbill();
  Helper h=new Helper();
  Widget ctm;
  CustomerInvoice ctm_=new CustomerInvoice();
  Future<void> TurnOfPopup(){
    Navigator.of(context, rootNavigator: true).pop("");

  }
  void AddItem(String code){
   // for(var i=0;i<15;i++) {
    //  code=i.toString();
      var c = new ItemBill();
      c.myctr.code = code;
      c.myctr.avtimg.myavt.avt =
      "https://new.monaco.vn/Images/Image/Calvin%20Klein/euphoria-women.jpg";
      c.myctr.name = "Eau De Parfum For Women";
      c.myctr.money = 1000000;
      c.myctr.qlt = 1;
      var ck = items.UpQltWidgetNote(code, 1, true);
      if (!ck) {
        c.myctr.total = 1000000;
        items.addWidgetNote(c);
      }
   // }
    ReCaculator();
  }
  void choosePhone(BuildContext context,String phone) {
    if(phone!=""){
      var api=new ApiApp();
      ctm_.myctr.name="Nguyễn Văn A";
      ctm_.myctr.sub=phone;
      ctm_.myctr.member="Member";
      ctm_.myctr.sumctm="100000";
      ctm_.myctr.setInfor();
      TurnOfPopup();

    }
  }
  void InputCode(BuildContext context) {
    final codeController = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
            content:
            Container(
              height: 120,
              child:
              Column(
                children: <Widget>[
                  TextField(
                    controller: codeController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.account_circle),
                      labelText: 'Code',
                    ),
                  ),
                  Container(
                    child: RaisedButton(
                      color: Colors.blue,
                      onPressed:(){

                        setState(() {
                          AddItem(codeController.text);
                          TurnOfPopup();
                        });
                        },
                      child: Text(
                          'Ok',
                          style: TextStyle(fontSize: 20)
                      ),
                    ),

                  )

                ],
              ),

            )

        ));
  }
  void InputGifCode(BuildContext context) {
    final codeController = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
            content:
            Container(
              height: 120,
              child:
              Column(
                children: <Widget>[
                  TextField(
                    controller: codeController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.account_circle),
                      labelText: 'Code quà tặng',
                    ),
                  ),
                  Container(
                    child: RaisedButton(
                      color: Colors.blue,
                      onPressed:(){
                        Map gif=new Map();
                        gif["code"]=codeController.text;
                        gif["val"]="50000";
                        ttinvoice.myctr.AddGif(gif,int.parse(ctm_.myctr.total));
                        TurnOfPopup();
                      },
                      child: Text(
                          'Ok',
                          style: TextStyle(fontSize: 20)
                      ),
                    ),

                  )

                ],
              ),

            )

        ));
  }
  void showAlert(BuildContext context) {
    final phoneController = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
            content:
            Container(
              height: 120,
              child:
              Column(
                children: <Widget>[
                  TextField(
                    controller: phoneController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.account_circle),
                      labelText: 'Phone number',
                    ),
                  ),
                  Container(
                    child: RaisedButton(
                      color: Colors.blue,
                      onPressed:()=> choosePhone(context,phoneController.text),
                      child: Text(
                          'Ok',
                          style: TextStyle(fontSize: 20)
                      ),
                    ),

                  )

                ],
              ),

            )

        ));
  }
  void ChooseSave(BuildContext context) {
    pay=new FormPay();
    final cardController = TextEditingController();
    cardController.text="";
    final ckController = TextEditingController();
    ckController.text="";
    final moneyController = TextEditingController();
    pay.myavt.total=ttinvoice.myctr.money;
    pay.myavt.wtotal=ttinvoice.myctr.money;
    moneyController.text="";

    showDialog(
        context: context,
        builder: (context) => AlertDialog(
            content:
            Container(
              color: Colors.transparent,
              height: 300,
              child:ListView(
                children: <Widget>[
                  Container(
                      child:
                      Column(
                        children: <Widget>[
                          pay,
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0,10,0,0),
                            child: RaisedButton(
                              color: Colors.blue,
                              onPressed:(){
                                SaveInvoice();
                              },
                              child: Text(
                                  'Save',
                                  style: TextStyle(fontSize: 20)
                              ),
                            ),
                          ),
                ],
              )
                        )

                ],
              ),

            )

        ));
  }
  void UpdateBill(BuildContext context,ItemBill it) {
    final qltController = TextEditingController();
    qltController.text=it.myctr.qlt.toString();
    var avtit=new CircleImg();
    avtit.myavt.avt=it.myctr.avtimg.myavt.avt;
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
            content:
            Container(
              height: 120,
              child:
              Column(
                children: <Widget>[
                  TextField(
                    controller: qltController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      icon: avtit,
                      labelText: 'Số lượng',
                    ),
                  ),
                  Container(
                      child:Row(
                        children: <Widget>[
                          RaisedButton(
                            color: Colors.blue,
                            onPressed:(){
                              setState(() {
                                items.UpQltWidgetNote(it.myctr.code,double.parse(qltController.text),false);
                                ReCaculator();
                                TurnOfPopup();
                              });

                            },
                            child: Text(
                                'Ok',
                                style: TextStyle(fontSize: 20)
                            ),
                          ),
                          Flexible(fit: FlexFit.tight, child: SizedBox()),
                          RaisedButton(
                            color: Colors.blue,
                            onPressed:(){
                              setState(() {
                              items.removeWidgetNote(it.myctr.code);
                              ReCaculator();
                              TurnOfPopup();

                              });
                            },
                            child: Text(
                                'Dellete',
                                style: TextStyle(fontSize: 20)
                            ),
                          ),
                        ],
                      )


                  )

                ],
              ),

            )

        ));
  }
  void ReCaculator(){
    int sumMn=0;
    for(var i in items.notes)
    {
      var m=i as ItemBill;
      sumMn=sumMn+m.myctr.total;
    }
    ctm_.myctr.total=sumMn.toString();
    ctm_.myctr.setInfor();
    ttinvoice.myctr.UpdateTotal(sumMn);
    //
  }
  void SaveInvoice() async{
    if(pay.myavt.wtotal>0)
      return ;
    setState(() {
      payment=true;
    });
    TurnOfPopup();
    List<Map> iv=new List<Map>();
    List<Map> mitem=new List<Map>();
    List<Map> mgif=new List<Map>();
    for(var i in items.notes){
      var n=i as ItemBill;
      Map j=new Map();
      j["code"]=n.myctr.code;
      j["qlt"]=n.myctr.qlt.toString();
      mitem.add(j);
    }
    for(var i in ttinvoice.myctr.gif){
      var n=i as LineGif;
      Map j=new Map();
      j["code"]=n.myctr.name;
      mgif.add(j);
    }
    Map o=new Map();
    o["shop"]="ndc";
    o["ctm"]=ctm_.myctr.name;
    o["phonectm"]=ctm_.myctr.sub;
    o["sumctm"]=ctm_.myctr.sumctm;
    o["member"]=ctm_.myctr.member;
    Map p=new Map();
    p["card"]=pay.myavt.cardController.text;
    p["money"]=pay.myavt.moneyController.text;
    p["bank"]=pay.myavt.ckController.text;
    p["return"]=pay.myavt.returnmn;
    iv.addAll(mitem);
    iv.addAll(mgif);
    iv.add(o);
    iv.add(p);

    var rs=await api.SaveInvoice(mitem,mgif,o);

    if(rs!="-1") {
      setState(() {
        payment = false;
        items = new ItemBillBloc();
        ttinvoice = new Totalbill();
        ctm_ = new CustomerInvoice();
      });

    }
  }
  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {

      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {

      });
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    var hd=new HeaderInvoice("SHD: 5555555555","339 NGUYỄN ĐÌNH CHIỂU","0908654773"),
    ctm=new GestureDetector(
      onTap:(){
        showAlert(context);
      },
      child:ctm_,

    );

  Widget ctroll(ItemBill o){
    CircleImg avtimg=new CircleImg();
    avtimg.myavt.avt=o.myctr.avtimg.myavt.avt;
     return GestureDetector(
       onTap: () {
         UpdateBill(context, o);
       },
      child:
      Padding(
        padding: const EdgeInsets.fromLTRB(0,0,0,8.0),
        child: new Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Container(
              child: avtimg,
            ),
        new Container(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(o.myctr.code),
              new Text(o.myctr.name),
              new Text("Giá bán: "+h.convertNumberToString(o.myctr.money.toString())),
            ],
          ),
        ),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            Text(o.myctr.qlt.toString()),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            Padding(
              padding: const EdgeInsets.fromLTRB(0,0,10,0),
              child: Text(h.convertNumberToString(o.myctr.total.toString())),
            )
          ],
        ),
      ),

    );
  }
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Bán Hàng"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0,0,0,0),
            child: Center(
              child: GestureDetector(
                child:Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: !payment?buttonCtr("Payment"):null,
                ),
                onTap: (){
                  if(!payment && items.notes.length>0)
                    ChooseSave(context);
                },
              ),
            ),
          )

        ],
      ),

      body:payment?Center(
        child:loaderCtr() ,
      ):Container(
        child: Column(
          children: <Widget>[
            hd,
            Padding(
              padding: const EdgeInsets.all(0.0),
              child: Container(

                  margin: const EdgeInsets.fromLTRB(5,0,5,5.0),
                  padding: const EdgeInsets.all(3.0),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                      border: Border.all(color: Colors.blueAccent)
                  ),
                  child:
                  Column(
                    children: <Widget>[
                      ctm,
                      ttinvoice,
                    ],
                  )

              ),

            ),

              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0,0,0,60),
                    child: ListView.builder(
                    controller: _controller,
                    itemCount:  items.notes.length,
                    itemBuilder: (context, index) {
                      var m=items.notes[index] as ItemBill;
                      return ctroll(m);
                    },
                ),
                  )
              )
              ,


          ],

        ),
      ),
      floatingActionButton:!payment?new Row(
        children: <Widget>[
          new Padding(
            padding: new EdgeInsets.symmetric(
              horizontal: 15.0,
            ),
          ), new Padding(
            padding: new EdgeInsets.symmetric(
              horizontal: 15.0,
            ),
          ),
          new FloatingActionButton(
            heroTag: "btn1",
            onPressed:() {
              InputCode(context);
            },
            child: new Icon(Icons.input),
          ),
          Flexible(fit: FlexFit.tight, child: SizedBox()),
          new FloatingActionButton(
            heroTag: "btn2",
            onPressed: () async {
              if(items.notes.length>0){
                InputGifCode(context);
              }
              // Add your onPressed code here!
            },
            child: Icon(Icons.gif),
            backgroundColor: Colors.green,
          ),
          Flexible(fit: FlexFit.tight, child: SizedBox()),
          new FloatingActionButton(
            heroTag: "btn3",
            onPressed: () async {
              var scan=ScanCode();
              String code=await scan.scanBarcodeNormal();
              if(code!="-1"){
                setState(() {
                  AddItem(code);
                });

              }
              // Add your onPressed code here!
            },
            child: Icon(Icons.camera_alt),
            backgroundColor: Colors.green,
          ),
        ],
      ):null,
    );


  }

}