import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'Database/Helper.dart';
import 'Database/SqlData.dart';
import 'package:async/async.dart';
import 'package:device_info/device_info.dart';
import 'package:get_version/get_version.dart';
class ApiApp {
  static String root=new Helper().hostsv;
  String urlsocket="http://192.168.137.1:8000";
  String url=root +"api/API/";
  String urlChat=root +"api/Chats/";
  String imgLink=root +"/images/MobileIcon/";
  String imgRoot=root +"/images/";
  Future<String> SaveInvoice(List<Map> product,List<Map> gif,Map infor) async {
    String rs="-1";
    var uri = Uri.parse(url+"SaveInvoice");
    var request = new http.MultipartRequest("POST", uri);

    request.fields["product"]=jsonEncode(product);
    request.fields["gif"]=jsonEncode(gif);
    request.fields["infor"]=jsonEncode(infor);
    var response = await request.send();
   await response.stream.transform(utf8.decoder).listen((value) {
      rs=value.toString();
    });
    return rs;
  }

  Future UploadProfile(File imageFile,String data) async {
    final prefs = await SharedPreferences.getInstance();
// Try reading data from the counter key. If it doesn't exist, return 0.
    //final idprofile = prefs.getInt('idprofile') ?? 0;
    final idprofile = prefs.getInt('idprofile');

    var uri = Uri.parse(url+"UploadProfile");
    var request = new http.MultipartRequest("POST", uri);
    try {
      var stream = new http.ByteStream(
          DelegatingStream.typed(imageFile.openRead()));
      var length = await imageFile.length();
      var multipartFile = new http.MultipartFile(
          'files', stream, length, filename: basename(imageFile.path));
      //contentType: new MediaType('image', 'png'));
      request.files.add(multipartFile);
    }
    catch(ex){}
    request.fields["data"]=data;
    request.fields["idprofile"]=idprofile.toString();
    var response = await request.send(); print(response.statusCode);
    response.stream.transform(utf8.decoder).listen((value) {

    });
  }
  Future<String> GetOSVersion() async{
    String platformVersion;
// Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await GetVersion.platformVersion;
    } catch(ex) {
      platformVersion = 'Failed to get platform version.';
    }
    return platformVersion;
  }

  Future<String> getDataLogin(String username,String pass) async {

    Map dv=new Map();
    dv["OSVersion"]=await GetOSVersion();
    try {

      var response = await http.get(
          Uri.encodeFull(url + "loginAndmenu?user=" + username + "&pass=" + pass + "&inforDevice=" + json.encode(dv) + ""),
          headers: {"Accept": "application/json"});

          var data = json.decode(response.body);

          if(data!="-1") {
            var rs = json.decode(data) as List;
             SharedPreferences prefs = await SharedPreferences.getInstance();
            await prefs.setInt('idprofile', int.parse(rs[0]["id"].toString()));
            SharedPreferences user_ = await SharedPreferences.getInstance();
            await user_.setString('user_name', rs[0]["username"].toString());
            SharedPreferences session = await SharedPreferences.getInstance();
            await session.setString('session', rs[0]["session"].toString());
          }
      return data;
    }
    catch(ex){
      return "-2";
    }

  }
  Future<String> getDataLogout() async {
    Helper h= new Helper();
    var session = await h.Getsession();
    var response = await http.get(
        Uri.encodeFull(url + "logout?session=" + session),
        headers: {"Accept": "application/json"});
  }
  Future<String> getUserChat() async {
     var response = await http.get(
          Uri.encodeFull(urlChat + "GetListChat"),
          headers: {"Accept": "application/json"});
      var data =json.decode(response.body);

      return data.toString();

  }
  Future<String> getItemScan(String code,String obj) async {
    try {
      var db=await SqlData();
      var lg=await db.getNote(1);
      var response = await http.get(
          Uri.encodeFull(url + "GetItem?code=" + code + "&obj=" + obj + ""),
          headers: {"Accept": "application/json"});
      var data = json.decode(response.body);

      return data;
    }
    catch(ex){
      return "-1";
    }

  }
  Future<String> getProfileLogin() async {
    try {
      var db=await SqlData();
      var lg=await db.getNote(1);
      var response = await http.get(
          Uri.encodeFull(url + "GetProFile?user=" + lg["user"].toString() + "&pass=" + lg["pass"].toString() + ""),
          headers: {"Accept": "application/json"});
      var data = json.decode(response.body);

      return data;
    }
    catch(ex){
      return "-1";
    }

  }
  Future<String> getDatalist(String type,String page,String number,String cols) async {
    try {
      var response = await http.get(
          Uri.encodeFull(url + "GetData?type="+type+"&page="+page+"&number="+number+"&cols="+cols+""),
          headers: {"Accept": "application/json"});
      var data = json.decode(response.body);
      return data;
    }
    catch(ex){
      return "-1";
    }

  }
  Future<String> getTodolist() async {
    try {
      SharedPreferences idpro = await SharedPreferences.getInstance();
      String idprof=await idpro.getInt('idprofile').toString();

      var response = await http.get(
          Uri.encodeFull(url + "GetTodolist?iduser=$idprof"),
          headers: {"Accept": "application/json"});
      var data = json.decode(response.body);
      return data;
    }
    catch(ex){
      return "-1";
    }

  }
  Future<String> GetIdLogin() async{
    SharedPreferences idprofile = await SharedPreferences.getInstance();
    return await idprofile.getInt('idprofile').toString();
  }
}