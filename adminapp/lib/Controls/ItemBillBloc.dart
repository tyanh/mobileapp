import 'dart:async';
import 'package:adminapp/Controls/ItemBill.dart';
import 'package:flutter/material.dart';


class ItemBillBloc {
  var notes =List<Widget>();
  var _controller = StreamController<List<Widget>>.broadcast();
  get controllerOut => _controller.stream.asBroadcastStream();
  get controllerIn => _controller.sink;
  addWidgetNote(Widget data) {
    notes.add(data);
    controllerIn.add(notes);

  }

  removeWidgetNote(String code) async {
    int o=0;
    for(var i in notes)
      {
        var m=i as ItemBill;
        if(m.myctr.code==code)
          {
            break;
          }
        o++;
      }

   await notes.removeAt(o);
   controllerIn.add(notes);

  }

  bool UpQltWidgetNote(String code,double qlt,bool plus) {
    for(var i in notes)
    {
      var m=i as ItemBill;
      if(m.myctr.code==code)
      {
        double q=qlt;
        int price=m.myctr.money;
        if(plus) {
          q=m.myctr.qlt;
          q = q + qlt;

        }
        String total=(q*price).round().toString();
        m.myctr.qlt = q;
        m.myctr.total=int.parse(total);
        return true;
        break;
      }

    }
    return false;
  }

}
