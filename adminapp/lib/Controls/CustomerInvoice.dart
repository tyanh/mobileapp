import 'package:adminapp/Database/Helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'CircleImg.dart';

class CustomerInvoice extends StatefulWidget{
  _CustomerInvoice myctr=new _CustomerInvoice();
  @override
  _CustomerInvoice createState() => myctr;
}

class _CustomerInvoice extends State<CustomerInvoice> {
  Helper h=new Helper();
  CircleImg avtimg=new CircleImg();
  String avt="";
  String name="Name";
  String sub="";
  String total="0";
  String sumctm="0";
  String member="";
  void setInfor(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    final leftSection = new Container(
      child: avtimg,
    );

    final middleSection = new Container(
       child: new Column(
        children: <Widget>[
          new Text(name),
          new Text(sub),
        ],
      ),
    );
    final rightSection = new Container(
      child: new Column(
        children: <Widget>[
          new Text("Doanh số:"+ h.convertNumberToString(sumctm)+" VNĐ"),
          new Text("Thẻ thành viên: "+member),
        ],
      ),
    );
    // TODO: implement build
    return Container(
      child:
      new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

            Row(
              children: <Widget>[
                leftSection,
                middleSection,
                Flexible(fit: FlexFit.tight, child: SizedBox()),
                rightSection,
              ],
            )
            ,
          Center(
            child:Text(h.convertNumberToString(total)+" VNĐ",style:TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20)),
          ),

        ],
      ),

    )
    ;
  }

}