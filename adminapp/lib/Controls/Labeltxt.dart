import 'package:flutter/material.dart';

class Labeltxt extends StatefulWidget {
  _Label myText=new _Label();

  @override
  _Label createState() => myText;
}
class _Label extends State<Labeltxt> {
  String t="";
  Future Settext(String newtxt) async{
    if(!mounted){
      t=newtxt;
      return;
    }
      setState(() {
        t=newtxt;
      });

  }
  void updateProfile(String newtxt) {
    setState(() => this.t=newtxt);
  }
  @override
  void initState() {
    print("initState");
     super.initState();

  }
  @override
  void didUpdateWidget(Widget oldWidget) {

  }
  @override
  void dispose() {
      super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return new Center(
        child:Text(t)
    );
  }
}