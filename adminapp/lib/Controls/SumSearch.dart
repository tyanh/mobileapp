import 'package:adminapp/Database/Helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class SumSearch extends StatefulWidget{
  _SumSearch myctr=new _SumSearch();
  @override
  _SumSearch createState() => myctr;
}

class _SumSearch extends State<SumSearch> {
  Helper h=new Helper();
  int discount=0;
  int money=0;

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final leftSection = new Container(
         child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Text("TỔNG GIẢM",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
            new Text(h.convertNumberToString(discount.toString())+" VNĐ")

          ],
        )

    );
    final middleSection = new Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Text("TỔNG SP",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
            new Text(h.convertNumberToString(discount.toString())+" VNĐ")

          ],
        )

    );
    final rightSection = new Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Text("THÀNH TIỀN",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
            new Text(h.convertNumberToString(money.toString())+" VNĐ")

          ],
        )
    );
    // TODO: implement build
    return Container(
      child:
      new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          leftSection,
          middleSection,
          rightSection
        ],
      ),

    )
    ;
  }

}