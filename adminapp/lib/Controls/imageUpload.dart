import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
class imageUpload extends StatefulWidget {
  ChooseFile myCtr=new ChooseFile();
  @override
  ChooseFile createState() => myCtr;

}

class ChooseFile extends State<imageUpload> {
  File _image;
  String link="assets/imgs/cloud_dowload.png";
  Future getImage() async {
    //var image = await ImagePicker.pickImage(source: ImageSource.camera);
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;

    });
  }
  File getFile(){
    return _image;
  }
  String bag(){

  }
  void Setimg(String l){
    setState(() {
      link=l;
    });
  }
  @override
  Widget build(BuildContext context) {

return  Center(
        child:new SizedBox(
          width: 200,
      height: 200,
      child: GestureDetector(
          onTap: getImage,
          child:
          CircleAvatar(

            backgroundImage:_image==null?NetworkImage (link):FileImage(_image),
            minRadius: 90,
            maxRadius: 150,
            backgroundColor: Colors.transparent,
          ),
          ),
        ),
);

  }
}