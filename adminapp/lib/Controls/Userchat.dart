import 'package:adminapp/Chat.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
class Userchat extends StatefulWidget {

  Userchats myuChat=new Userchats();
  @override
  Userchats createState() => myuChat;

}
class Userchats extends State<Userchat> {
  //final GlobalKey<GestureDetector> _scraffkey=new GlobalKey<ScaffoldState>();
  String name;
  String idclient;
  String ipclient;
  String iduser;
  bool dlbage=false;
  bool online=false;
  updateItem(bool st,String idclient,String ipclient){
        setState(() {
          online=st;
      idclient=idclient;
      ipclient=ipclient;
    });
    print(this.online);
  }
  @override
  void initState() {
    print("initState");
       super.initState();
  }
  @override
  didUpdateWidget(Userchat oldWidget){
    print("didUpdateWidget");

    super.didUpdateWidget(oldWidget);
   }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("***************didChangeAppLifecycleState***************");
    print('state = $state');
  }
  @override
  void deactivate() {
    super.deactivate();
    print("***************deactive***************");
  }
  @override
  void dispose() {
    super.dispose();
    print("***************dispose***************");
  }
  @override
  void reassemble() {
    super.reassemble();
    print("***************reassemble***************");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  GestureDetector(
      onTap:(){

        setState(() {
          online?online=false:online=true;
          print(online);
          idclient=idclient;
          ipclient=ipclient;
        });
        //if(online) {
         // var chat = new Chat();
          //chat.myChat.namechat = name;
         // Navigator.push(
          //  context,
          //  MaterialPageRoute(builder: (context) => chat),
         // );
       // }
      } ,
      child: ListTile(

        leading:
        Badge(
          badgeContent: Text("new"),
          showBadge:dlbage,
          child: Icon(Icons.account_circle),
        ),
        title:(online)?Text(name):Text(name,style:TextStyle(color: Colors.black.withOpacity(0.3))),
      ),
    );
  }

}
