import 'package:adminapp/Database/Helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'CircleImg.dart';

class LineGif extends StatefulWidget{
  _LineGif myctr=new _LineGif();
  @override
  _LineGif createState() => myctr;
}

class _LineGif extends State<LineGif> {
  Helper h=new Helper();
  String name="Name";
  int money=0;
  String uuid = new DateTime.now().millisecondsSinceEpoch.toString();
  void setInfor(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    final leftSection = new Container(
      child:  new Text(name),
    );

    final middleSection = new Container(
      child: new Text(h.convertNumberToString(money.toString())),
    );
    final rightSection = new Container(
      alignment: Alignment.topRight,
      child: Icon(Icons.remove),
    );
    // TODO: implement build
    return Container(
      child:
      new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          leftSection,
          middleSection,
          rightSection
        ],
      ),

    )
    ;
  }

}