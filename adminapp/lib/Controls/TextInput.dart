import 'dart:async';
import 'package:flutter/material.dart';
class TextInput extends StatelessWidget {
  String hintext="";
  bool obscureText=true;
  var control=new TextEditingController();
  TextInput(this.hintext,this.control,this.obscureText);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child:Container(child:
          Padding(
            padding: const EdgeInsets.fromLTRB(20.0,0,0,0),
            child: Text(hintext),
          ),width: double.infinity,),
        ),
    ListTile(
       title: TextField(
        obscureText: obscureText,
        controller: control,
        decoration: InputDecoration(
            focusColor: Colors.red,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "",
            border:
            OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      ),
    ),
    ]
    );
  }
}