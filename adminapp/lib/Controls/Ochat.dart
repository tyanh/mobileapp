import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Ochat extends StatelessWidget {
  String title = "";
  Ochat(this.title);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
    Padding(
      padding: const EdgeInsets.all(8.0),
      child:
      Align(
        alignment: Alignment.bottomRight,
        child:
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SizedBox(
              width: 300,
                child:Text(title, textAlign: TextAlign.end,)),
            Icon(Icons.account_circle),
          ],
        )

      ),
    );
  }
}