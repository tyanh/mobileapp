import 'package:adminapp/Controls/LineGif.dart';
import 'package:adminapp/Database/Helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'CircleImg.dart';

class Totalbill extends StatefulWidget{
  _Totalbill myctr=new _Totalbill();
  @override
  _Totalbill createState() => myctr;
}

class _Totalbill extends State<Totalbill> {
  Helper h=new Helper();
  int discount=0;
  int money=0;
  List<LineGif> gif=new List<LineGif>();
  List<Map> dc=new List<Map>();
  void AddGif(Map m,int mn){
    int a=int.parse(m["val"]);
    discount=discount+a;
    money=mn-discount;
    dc.add(m);
    LineGif g=new LineGif();
    g.myctr.name=m["code"];
    g.myctr.money=int.parse(m["val"]);
    gif.add(g);
    setState((){});
  }
  Widget MyCtr(String o,int money,String uuid){
    LineGif g=new LineGif();
    g.myctr.name=o;
    g.myctr.money=money;
    g.myctr.uuid=uuid;
    var ges=new GestureDetector(
      onTap:(){
        removeGif(g.myctr.uuid);
      },
      child:g,

    );
    return ges;
  }
  void removeGif(String o){
    int od=0;
    int val=0;
    for(var i in gif)
      {
        var m=i as LineGif;
        if(m.myctr.uuid==o) {
          val=m.myctr.money;
          break;
        }
        od++;
      }
    gif.removeAt(od);
    discount=discount-val;
    money=money+val;
    Navigator.of(context, rootNavigator: true).pop("");
    ListGif(context);
    setState((){});
  }
  void UpdateTotal(int mn) async{
    money=mn-discount;
   await setState(() {});
  }
  void ListGif(BuildContext context) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
              content:
              Container(
                height: 120,
                child:
                ListView.builder(
                  itemCount:  gif.length,
                  itemBuilder: (context, index) {
                     return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:
                          MyCtr(gif[index].myctr.name,gif[index].myctr.money,gif[index].myctr.uuid),
                        ),
                        // Widget to display the list of project
                      ],
                    );
                  },
                )


              )

          ));


  }
  void setInfor(){
    setState(() {

    });
  }
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final leftSection = new GestureDetector(
      onTap: (){
          ListGif(context);

      },
       child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Text("TỔNG GIẢM",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          new Text(h.convertNumberToString(discount.toString())+" VNĐ")

        ],
      )

    );
    final rightSection = new Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Text("THÀNH TIỀN",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            new Text(h.convertNumberToString(money.toString())+" VNĐ")

          ],
        )
    );
    // TODO: implement build
    return Container(
      child:
      new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          leftSection,
          rightSection
        ],
      ),

    )
    ;
  }

}