import 'package:flutter/cupertino.dart';

import 'CircleImg.dart';

class HeaderInvoice extends StatelessWidget {
  CircleImg avtimg=new CircleImg();
  String linkavt="http://icons.iconarchive.com/icons/blackvariant/button-ui-requests-6/256/Vmware-icon.png";
  String IvNumber;
  String address;
  String phone;
  HeaderInvoice(this.IvNumber,this.address,this.phone);
  var now=new DateTime.now();

  @override
  Widget build(BuildContext context) {
    avtimg.myavt.avt=linkavt;
    String n=now.day.toString()+"/"+now.month.toString()+"/"+now.year.toString()+"\n"+IvNumber;
    final leftSection = new Container(
      child: avtimg,
    );
    final middleSection = new Container(
      child: new Column(
        children: <Widget>[
          new Text(address,style: TextStyle(fontSize: 10),),
          new Text(phone,style: TextStyle(fontSize: 10)),
        ],
      ),
    );
    final rightSection = new Container(
      alignment: Alignment.topRight,
      child: new Text(n,style: TextStyle(fontSize: 10),textAlign: TextAlign.center,),
    );
    return Container(
      child:
      Padding(
        padding: const EdgeInsets.fromLTRB(0,0,0,0),
        child: new Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            leftSection,
            middleSection,
             Flexible(fit: FlexFit.tight, child: SizedBox()),
             Padding(
               padding: const EdgeInsets.fromLTRB(0,0,10,0),
               child: rightSection,
             ),
          ],
        ),
      ),

    );
  }
}