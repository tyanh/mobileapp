import 'package:flutter/material.dart';
import 'package:slider_button/slider_button.dart';
import '../Login.dart';
class SLButton extends StatelessWidget {
  String t="";
  SLButton(this.t){}
  @override
  Widget build(BuildContext context) {
    return new Column(
        children: [
          new SliderButton(
            action: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Login()),
              );
            },
             label: Text(
              t,
              style: TextStyle(
                  color: Color(0xff4a4a4a),
                  fontSize: 10),
            ),
            icon: Center(
                child: Icon(
                  Icons.account_circle,
                  color: Colors.white,
                  size: 20.0,
                  semanticLabel: 'Text to announce in accessibility modes',
                )),

            ///Change All the color and size from here.
            width: 230,
            radius: 10,
            buttonColor: Color(0xffd60000),
            backgroundColor: Color(0xff534bae),
            highlightedColor: Colors.white,
            baseColor: Colors.red,
          ),
        ]
    );

  }
}
