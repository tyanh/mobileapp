import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CircleImg extends StatefulWidget{
  _CircleImg myavt=new _CircleImg();
  @override
  _CircleImg createState() => myavt;
}

class _CircleImg extends State<CircleImg> {
  String avt="";
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return IconButton(
      icon:avt!=""?
      CircleAvatar(
        radius: 15.0,
        backgroundImage:NetworkImage(avt),

      ):Icon(Icons.account_circle),
      onPressed: () {

      },
    );
  }

}