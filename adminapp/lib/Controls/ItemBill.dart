import 'package:adminapp/Database/Helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'CircleImg.dart';
class ItemBill extends StatefulWidget{
  _ItemBill myctr=new _ItemBill();
  @override
  _ItemBill createState() => myctr;
}

class _ItemBill extends State<ItemBill> {
  CircleImg avtimg=new CircleImg();
  Helper h=new Helper();
  String code="";
  String name="";
  double qlt=0;
  int money=0;
  int total=0;
  void setInfor(){
    setState(() {

    });
  }
  @override
  void initState() {
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    final leftSection = new Container(
      child: avtimg,
    );

    final middleSection = new Container(
      child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
         children: <Widget>[
          new Text(code),
          new Text(name),
          new Text("Giá bán: "+h.convertNumberToString(money.toString())),
        ],
      ),
    );
    final rightSection = new Container(
      alignment: Alignment.topRight,
      child: new Text(h.convertNumberToString(total.toString())),
    );
    // TODO: implement build
    return Container(
      child:
      Padding(
        padding: const EdgeInsets.fromLTRB(0,0,0,8.0),
        child: new Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            leftSection,
            middleSection,
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            Text(qlt.toString()),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            Padding(
              padding: const EdgeInsets.fromLTRB(0,0,10,0),
              child: rightSection,
            )
          ],
        ),
      ),

    )
    ;
  }

}