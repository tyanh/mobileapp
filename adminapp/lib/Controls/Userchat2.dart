import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Chat.dart';

class Userchat2 extends StatelessWidget {
  String name;
  String idclient;
  String ipclient;
  String iduser;
  bool dlbage=false;
  bool online=false;

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap:(){
        if(online) {
          var chat = new Chat();
          chat.myChat.namechat = name;
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => chat),
          );
        }
      } ,
      child: ListTile(

        leading:
        Badge(
          badgeContent: Text("new"),
          showBadge:dlbage,
          child: Icon(Icons.account_circle),
        ),
        title:(online)?Text(name):Text(name,style:TextStyle(color: Colors.black.withOpacity(0.3))),
      ),
    );
  }
}