import 'package:flutter/material.dart';
class ListTileSale extends StatelessWidget {
  String title;
  String number;
  @override
  Widget build(BuildContext context) {
    return
      ListTile(
        leading: Text(number),
        title: Text(title),
      );
  }
}