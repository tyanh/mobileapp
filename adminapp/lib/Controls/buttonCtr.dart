import 'package:flutter/material.dart';

class buttonCtr extends StatelessWidget {
  String title="";

  buttonCtr(String title){
    this.title=title;
  }
  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    return new SizedBox(
       child: Padding(
         padding: const EdgeInsets.fromLTRB(15.0,5.0,15.0,5.0),
         child: Material(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(22.0)),
          color: Color(0xFF801E48),
          child: MaterialButton(

            height: 30.0,
            child: new Text(title,
                style:
                new TextStyle(fontSize: 16.0, color: Colors.white)),
          ),
      ),
       ),
    );
  }
}