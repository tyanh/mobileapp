import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Mechat extends StatelessWidget {
  String user = "";
  String content = "";
  Mechat(this.user,this.content);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
           Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child:Icon(Icons.art_track),
              ),

                 Container(
                  margin: const EdgeInsets.all(0),
                  width: 300,
                  child:
                    ListTile(
                      title: Text(user),
                      subtitle: Text(content),
                    ),

                ),

            ],
          );



  }
}