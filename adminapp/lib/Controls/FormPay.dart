import 'package:adminapp/Database/Helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormPay extends StatefulWidget{
  _FormPay myavt=new _FormPay();
  @override
  _FormPay createState() => myavt;
}

class _FormPay extends State<FormPay> {
  Helper h=new Helper();
  final cardController = TextEditingController();
  final ckController = TextEditingController();
  final moneyController = TextEditingController();
  int total=0;
  int wtotal=0;
  int returnmn=0;

  @override
  Widget build(BuildContext context) {
    onchangeInput(){
    wtotal=total;
    returnmn=0;
    int a=h.convertStringToInt(cardController.text.toString());
    int b=h.convertStringToInt(ckController.text.toString());
    int c=h.convertStringToInt(moneyController.text.toString());
    int sum=a+b+c;
    print(wtotal);
    print(sum);
    FocusScope.of(context).requestFocus(new FocusNode());
    setState(() {

      if(sum>wtotal) {
        returnmn = sum-wtotal;
        wtotal=0;
      }
      else
        wtotal=wtotal-sum;
    });
  }

    // TODO: implement build
    return   Container(
            child:
            Column(
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.fromLTRB(0,10,0,0),
                    child: Align(
                      alignment: Alignment.center,
                      child: Column(  children: <Widget>[
                        Text(h.convertNumberToString(wtotal.toString()),textAlign: TextAlign.right,),
                      ],),

                    )

                ),
                TextField(
                  onEditingComplete: () {
                    onchangeInput();
                  },
                  controller: cardController,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                    labelText: 'Cà thẻ',
                  ),
                ),
                TextField(
                  onEditingComplete: () {
                    onchangeInput();
                  },
                  controller: ckController,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                    labelText: 'Chuyển khoản',
                  ),
                ),
                TextField(
                  onEditingComplete: () {
                    onchangeInput();
                  },
                  controller: moneyController,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                    labelText: 'Tiền mặt',
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.fromLTRB(0,10,0,0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Column(  children: <Widget>[
                        Text("Tiền Thối: "+h.convertNumberToString(returnmn.toString()),textAlign: TextAlign.right,),
                      ],),

                    )

                ),

              ],
            )


        );


  }

}