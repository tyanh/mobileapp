import 'package:adminapp/Database/Helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'CircleImg.dart';
class ItemSearchInvoice extends StatefulWidget{
  _ItemSearchInvoice myctr=new _ItemSearchInvoice();
  @override
  _ItemSearchInvoice createState() => myctr;
}

class _ItemSearchInvoice extends State<ItemSearchInvoice> {
  CircleImg avtimg=new CircleImg();
  Helper h=new Helper();
  String NumInvoice;
  String name="";
  String phone="";
  double qlt=0;
  int summoney=0;
  int money=0;
  int order=0;
  String member;
  @override
  void initState() {
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    final leftSection = new Container(
      child: Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: CircleAvatar(
          radius: 15.0,
          child:Text(order.toString()),

        ),
      ),
    );

    final middleSection = new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(name+" - "+phone),
          new Text("Doanh số: "+h.convertNumberToString(summoney.toString())),
          new Text("Thẻ Thành Viên: "+member),
        ],
      ),
    );
    final rightSection = new Container(
      alignment: Alignment.topRight,
      child: new Text(h.convertNumberToString(money.toString())),
    );
    // TODO: implement build
    return Container(
      child:
      Padding(
        padding: const EdgeInsets.fromLTRB(0,0,0,8.0),
        child: new Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[

            leftSection,
            middleSection,
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            Text(qlt.toString()),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            Padding(
              padding: const EdgeInsets.fromLTRB(0,0,10,0),
              child: rightSection,
            )
          ],
        ),
      ),

    )
    ;
  }

}