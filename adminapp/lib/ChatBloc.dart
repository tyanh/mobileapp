import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:adminapp/Controls/Mechat.dart';
import 'package:flutter/material.dart';

import 'Controls/Ochat.dart';
import 'nSocketIo.dart';

class ChatBloc {
  final notes =List<Widget>();
  final _controller = StreamController<List<Widget>>.broadcast();
  get controllerOut => _controller.stream.asBroadcastStream();
  get controllerIn => _controller.sink;

  addFriendNote(String data) {
    var j=jsonDecode(data);
    var c=new Mechat(j["username"].toString(),j["content"].toString());
    notes.add(c);
    controllerIn.add(notes);
  }
  addMeNote(String txt) {
    var c=new Ochat(txt);
    notes.add(c);
    controllerIn.add(notes);
  }
  void dispose() {
    print("dispose");
    _controller.close();
  }
}
class Note {
  String title, note;
  Note(this.title, this.note);
}