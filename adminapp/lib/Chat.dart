import 'dart:async';

import 'package:adminapp/Controls/Mechat.dart';
import 'package:adminapp/nSocketIo.dart';
import 'package:adminapp/socketIoClient.dart';
import 'package:flutter/material.dart';

import 'ChatBloc.dart';
class Chat extends StatefulWidget{
  _Chat myChat=new _Chat();
  @override
  _Chat createState() => myChat;

  //@override
  //State<StatefulWidget> createState() {
    // TODO: implement createState
   // return _Chat();
  //}
}

class _Chat extends State<Chat> {
  var txt = TextEditingController();
  String namechat;
  String idchat;
  ChatBloc Chats = ChatBloc();
  socketIoClient sk=new socketIoClient();
  @override
  Widget build(BuildContext context) {
    sk.Chats=Chats;
    // TODO: implement build
    return
        Scaffold(
            appBar: AppBar(
              title: Text(namechat),

            ),
            body:Container(

            child: new Column(
             mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(

                    child:
                    GestureDetector(

                      child: StreamBuilder(
                      //initialData:List<Mechat>,
                      stream: Chats.controllerOut,
                      builder: (BuildContext context, snapShot) => Center(
                        child: Container(
                            child:(!snapShot.hasData)?Text("Loading.."):
                            ListView.builder(
                              itemCount:   snapShot.data.length,
                              itemBuilder: (context, index) {
                               return Column(
                                  children: <Widget>[
                                    snapShot.data[index],
                                    // Widget to display the list of project
                                  ],
                                );
                              },
                            )


                        ),
                      ),
                    ),
        ),
                ),
                Expanded(
                      child:Center(

                      child: Align(
                          alignment: FractionalOffset.bottomCenter,
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 10.0),
                            child:new Row(
                              children: <Widget>[
                                Expanded(
                               child: TextField(
                                 controller: txt,
                                  obscureText: false,
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                      hintText: "Content chat",
                                      border:
                                      OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                                ),
                            ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child:
                                  new IconButton(
                                    icon: new Icon(Icons.send),
                                    highlightColor: Colors.pink,
                                    onPressed:(){

                                      sk.sendmess("messages",txt.text);
                                     // txt.text="";
                                      FocusScope.of(context).requestFocus(new FocusNode());
    } ,
                                  ),

                                ),
                              ],

                            ),
                             //Your widget here,
                          )
                      ),
                    )
                )

              ],
            )
          ),
                  // ,
        );

  }

}


