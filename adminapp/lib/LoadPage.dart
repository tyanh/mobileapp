import 'package:flutter/material.dart';
import 'dart:async';
import 'Controls/SLButton.dart';
import 'ApiApp.dart';
class LoadPage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
      return Loader();
  }

}
class Loader extends State<LoadPage>{

    @override
  // TODO: implement widget
  List<Color> colors = [
    Colors.red,
    Colors.green,
    Colors.indigo,
    Colors.pinkAccent,
    Colors.blue
  ];
  Widget build(BuildContext context){

    return MaterialApp(
      home:new ColorLoader(colors: colors,duration: Duration(milliseconds: 1200))
    );
  }

}
class ColorLoader extends StatefulWidget {
  final List<Color> colors;
  final Duration duration;
  ColorLoader({this.colors, this.duration});

  @override
  _ColorLoaderState createState() =>
      _ColorLoaderState(this.colors, this.duration);

}
bool checkConnection(){
  bool Connection = true;
  return Connection;
}
class _ColorLoaderState extends State<ColorLoader> with SingleTickerProviderStateMixin {
  final List<Color> colors;
  final Duration duration;
  bool connect=checkConnection();
  Timer timer;
  _ColorLoaderState(this.colors, this.duration);

  //noSuchMethod(Invocation i) => super.noSuchMethod(i);

  List<ColorTween> tweenAnimations = [];
  int tweenIndex = 0;

  AnimationController controller;
  List<Animation<Color>> colorAnimations = [];

  @override
  void initState()  {
    super.initState();
    controller = new AnimationController(
      vsync: this,
      duration: duration,
    );

    for (int i = 0; i < colors.length - 1; i++) {
      tweenAnimations.add(ColorTween(begin: colors[i], end: colors[i + 1]));
    }

    tweenAnimations
        .add(ColorTween(begin: colors[colors.length - 1], end: colors[0]));

    for (int i = 0; i < colors.length; i++) {
      Animation<Color> animation = tweenAnimations[i].animate(CurvedAnimation(
          parent: controller,
          curve: Interval((1 / colors.length) * (i + 1) - 0.05,
              (1 / colors.length) * (i + 1),
              curve: Curves.linear)));

      colorAnimations.add(animation);
    }

    tweenIndex = 0;
    timer = Timer.periodic(duration, (Timer t) {
      setState(() {
        tweenIndex = (tweenIndex + 1) % colors.length;
      });
    });

    controller.forward();

  }

  @override
  Widget build(BuildContext context)  {
    var api=new ApiApp();
    AssetImage img=AssetImage('assets/imgs/logo.png');
    Widget loader;
    if(connect) {
      loader = CircularProgressIndicator(
        strokeWidth: 5.0,
        valueColor: colorAnimations[tweenIndex],
      );

    }
    else
      {
        loader= GestureDetector(
          child: SLButton("Not internet"),
         );
      }
    return Container(
     child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.network(api.imgLink+"/logo.png"),
          loader,

        ],

      ),

      color:Colors.white,
    );

  }


  @override
  void dispose() {
    timer.cancel();
    controller.dispose();
    super.dispose();
  }
}