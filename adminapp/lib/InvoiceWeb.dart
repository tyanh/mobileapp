import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'Controls/buttonCtr.dart';
import 'Database/Helper.dart';
import 'ScanCode.dart';
class InvoiceWeb extends StatefulWidget {
    // TODO: implement createState
    @override
    _InvoiceWeb createState() => new _InvoiceWeb();

}
class _InvoiceWeb extends State<InvoiceWeb> {
  Helper h=new Helper();
  Completer<WebViewController> _controller = Completer<WebViewController>();
  WebViewController _myController;
  String url="";
  void Bindurl() async  {
    var session = await h.Getsession();
    url=h.hostsv+"MobileLayout/InvoicesSale";
    setState((){
    });
  }
  @override
  void initState(){
      Bindurl();
     super.initState();

  }
  @override
  Widget build(BuildContext context)  {
    // TODO: implement build
    return (url=="")?null:Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Bán Hàng 23"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0,0,0,0),
            child: Center(
              child: GestureDetector(
                child:Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: buttonCtr("Payment"),
                ),
                onTap: (){
                  _myController.evaluateJavascript('openpayment()');
                  
                },
              ),
            ),
          )

        ],
      ),
        body: new WebView(
          initialUrl:url,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _myController=webViewController;
            _controller.complete(webViewController);

          },
        ),
        floatingActionButton:
        new Row(
          children: <Widget>[
            new Padding(
              padding: new EdgeInsets.symmetric(
                horizontal: 15.0,
              ),
            ),
            new FloatingActionButton(
              heroTag: "btn1",
              onPressed:() {
                _myController.evaluateJavascript('addPro()');
              },
              child: new Icon(Icons.input),
            ),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            new FloatingActionButton(
              heroTag: "btn2",
              onPressed: () async {
                _myController.evaluateJavascript('gift()');

                // Add your onPressed code here!
              },
              child: Icon(Icons.gif),
              backgroundColor: Colors.green,
            ),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            new FloatingActionButton(
              heroTag: "btn3",
              onPressed: () async {
                var scan=ScanCode();
                String code=await scan.scanBarcodeNormal();
                if(code!="-1")
                  _myController.evaluateJavascript('InsertPro('+code+')');
                // Add your onPressed code here!
              },
              child: Icon(Icons.camera_alt),
              backgroundColor: Colors.green,
            ),
          ],
        )

    );
  }

}

