import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_mobile_vision/qr_camera.dart';
class scantool extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _scantool();
  }
}
class _scantool extends State<scantool> {
  bool scan=true;
  String code_="";
  _qrCallback(String code) {
    code_=code;
    setState(() {
      scan=false;
    });
    Navigator.pop(context,code_);
  }
  // TODO: implement build

  @override
  Widget build(BuildContext context) {
    var cmr=new QrCamera(
        qrCodeCallback: (code) {
          if(scan)
          _qrCallback(code);
      },
    );
    
    return new
    Scaffold(
      body:
    SizedBox(
        child:cmr,
    ),
      floatingActionButton: new FloatingActionButton(
          child: new Text(
            "back",
            textAlign: TextAlign.center,
          ),
          onPressed: () {
            Navigator.pop(context,'-1');
          }),
    );
  }
}