import 'dart:async';

import 'package:adminstore/Helper/linkandvariable.dart';
import 'package:adminstore/Pages/Home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';


class login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _login();
  }
}
class _login extends State<login> {

  linkandvariable h=new linkandvariable();
  int _selectedIndex =0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    String url=h.hostsv+ "MobileLayout/Login";
    Completer<WebViewController> _controller = Completer<WebViewController>();
    JavascriptChannel _redirectHome(BuildContext context){
      return JavascriptChannel(
          name:"redirectHome",
          onMessageReceived: (JavascriptMessage message){
            print(message);
            runApp(MaterialApp(
              home: Home(),
            ));
          }
      );
    }
    return MaterialApp(
        home:Scaffold(
          body: new WebView(
            initialUrl: url,
            javascriptMode: JavascriptMode.unrestricted,
            javascriptChannels: <JavascriptChannel>[
              _redirectHome(context),
            ].toSet(),
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
          ),
        )
    );

  }
}
