import 'dart:async';
import 'package:adminstore/Controls/buttonCtr.dart';
import 'package:adminstore/Controls/scantool.dart';
import 'package:adminstore/Helper/linkandvariable.dart';
import 'package:flutter/material.dart';
import 'package:qr_mobile_vision/qr_camera.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Sale extends StatefulWidget {
  // TODO: implement createState
  @override
  _Sale createState() => new _Sale();

}
class _Sale extends State<Sale> {
  linkandvariable h=new linkandvariable();
  Completer<WebViewController> _controller = Completer<WebViewController>();
  WebViewController _myController;
  String url="";
  void Bindurl() async  {
    url=h.hostsv+"MobileLayout/InvoicesSale";
    setState((){
    });
  }
  @override
  void initState(){
    Bindurl();
    super.initState();

  }
  @override
  Widget build(BuildContext context)  {

    // TODO: implement build
    return (url=="")?Text(""):Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text("Bán Hàng"),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0,0,0,0),
              child: Center(
                child: GestureDetector(
                  child:Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: buttonCtr("Payment"),
                  ),
                  onTap: (){
                    _myController.evaluateJavascript('openpayment()');

                  },
                ),
              ),
            )

          ],
        ),
        body: new WebView(
          initialUrl:url,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _myController=webViewController;
            _controller.complete(webViewController);

          },
        )
        ,floatingActionButton:
        new Row(
          children: <Widget>[
            new Padding(
              padding: new EdgeInsets.symmetric(
                horizontal: 15.0,
              ),
            ),
            new FloatingActionButton(
              heroTag: "btn1",
              onPressed:() {
                _myController.evaluateJavascript('addPro()');
              },
              child: new Icon(Icons.input),
            ),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            new FloatingActionButton(
              heroTag: "btn2",
              onPressed: () async {
                _myController.evaluateJavascript('gift()');

                // Add your onPressed code here!
              },
              child: Icon(Icons.gif),
              backgroundColor: Colors.green,
            ),
            Flexible(fit: FlexFit.tight, child: SizedBox()),
            new FloatingActionButton(
              heroTag: "btn3",
              onPressed: () async{
                String code = await Navigator.push(context, MaterialPageRoute(builder: (context) => scantool()));
                print(code);
                if(code!="-1")
                 _myController.evaluateJavascript('InsertPro('+code+')');
                // Add your onPressed code here!
              },
              child: Icon(Icons.camera_alt),
              backgroundColor: Colors.green,
            ),
          ],
        )

    );
  }

}