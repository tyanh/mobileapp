import 'dart:async';
import 'package:adminstore/Controls/buttonCtr.dart';
import 'package:adminstore/Controls/imageUpload.dart';
import 'package:adminstore/Helper/linkandvariable.dart';
import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class Product extends StatefulWidget {
  // TODO: implement createState
  String link="";
  String title="";
  Product(this.link,this.title);
  @override
  _Product createState() => new _Product(link,title);

}
class _Product extends State<Product> {

  linkandvariable h=new linkandvariable();
  Completer<WebViewController> _controller = Completer<WebViewController>();
  WebViewController _myController;
  String link="";
  String title="";
  String url="";
  _Product(this.link,this.title);
  void Bindurl() async  {
    url=link;
    print(url);
    url=h.hostsv+url;
    setState((){
    });
  }
  @override
  void initState(){

    Bindurl();
    super.initState();

  }

  @override
  Widget build(BuildContext context)  {
    var file=new imageUpload();
    // TODO: implement build
    return (url=="")?null:Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text(title),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0,0,0,0),
              child: Center(
                child: GestureDetector(
                  child:Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: buttonCtr("Add Image"),
                  ),
                  onTap:() async{
                      String p=await file.getFile();
                    _myController.evaluateJavascript('imgFuttersJquery("'+p+'")');

                  },
                ),
              ),
            )

          ],
        ),
        body: new WebView(
          initialUrl:url,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _myController=webViewController;
            _controller.complete(webViewController);

          },
        ),

    );
  }

}

