import 'dart:async';
import 'dart:convert';
import 'package:adminstore/Controls/imageUpload.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../Helper/linkandvariable.dart';
import 'dart:io';

import 'Product.dart';
class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoadContend();
  }
}
class LoadContend extends State<Home> {

  linkandvariable h=new linkandvariable();
  int _selectedIndex =0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    String url=h.hostsv+"MobileLayout/Login";
    Completer<WebViewController> _controller = Completer<WebViewController>();
    List<BottomNavigationBarItem> btmenu = new List<BottomNavigationBarItem>();
    btmenu.add(BottomNavigationBarItem(
      icon: Icon(Icons.home),
      title: Text('Home'),
    ),
    );
    btmenu.add(BottomNavigationBarItem(
      icon: Icon(Icons.local_grocery_store),
      title: Text('Bán hàng'),

    ),
    );
    btmenu.add(BottomNavigationBarItem(
      icon: Icon(Icons.search),
      title: Text('Kiểm hàng'),
    ),
    );
    JavascriptChannel receivingdata(BuildContext context){
      return JavascriptChannel(
        name:"redirectPage",
        onMessageReceived: (JavascriptMessage message){
          Map<String, dynamic> ms = jsonDecode(message.message);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>new Product(ms["link"],ms["title"])),
          );
        },
      );
    }

    return MaterialApp(
      home:Scaffold(
        body: new WebView(
        initialUrl: url,
        javascriptMode: JavascriptMode.unrestricted,
        javascriptChannels: <JavascriptChannel>[
            receivingdata(context),
          ].toSet(),
        onWebViewCreated: (WebViewController webViewController) {
             _controller.complete(webViewController);
        },
      ),

      )
    );

  }
}
